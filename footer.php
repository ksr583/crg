<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */
?>

	</div><!-- #main -->

	<footer id="colophon" role="contentinfo">
	
			<nav id="footer"><a href="/" title="Home">Home</a> &nbsp;|&nbsp; <a href="/products/" title="Products">Our Products</a> &nbsp;|&nbsp; <a href="/services/" title="Services">Our Services</a> &nbsp;|&nbsp; <a href="/category/calvins-corner/" title="Calvin's Corner">Calvin's Corner</a> &nbsp;|&nbsp; <a href="/contact-us/" title="Contact Us">Contact Us</a> 800.633.6272</nav>
			<div id="copyright">&copy; <?php the_time('Y') ?> Channeled Resources Group</div>

			<?php
				/* A sidebar in the footer? Yep. You can can customize
				 * your footer with three columns of widgets.
				 */
				if ( ! is_404() )
					get_sidebar( 'footer' );
			?>


	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>